package com.lottostarlet.backendtest;


import static org.mockito.Matchers.isNull;

import org.hamcrest.core.IsNot;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;

import com.lottostarlet.backendtest.app.persistence.GameResultRepository;
import com.lottostarlet.backendtest.app.service.BackendService;

/**
 * 
 * @author Davide Martorana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ApplicationTest {

	@Autowired
	private WebApplicationContext context;
	
	@Test
	public void validContextTest() {
		Assert.assertThat(this.context, IsNot.not(isNull()));
		
		Assert.assertThat(this.context.getBean(BackendService.class), IsNot.not(isNull()));		
		Assert.assertThat(this.context.getBean(GameResultRepository.class), IsNot.not(isNull()));
	}
	
}
