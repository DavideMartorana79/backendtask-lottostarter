package com.lottostarlet.backendtest;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.lottostarlet.backendtest.app.config.SecurityConfig;
import com.lottostarlet.backendtest.app.controllers.BackendController;
import com.lottostarlet.backendtest.app.domain.GameResultType;
import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.model.GameResult;
import com.lottostarlet.backendtest.app.persistence.GameResultRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(BackendController.class)
@ContextConfiguration(classes={BackendControllerTest.Config.class})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class BackendControllerTest {
	
	@EnableWebMvc
	@ComponentScan(basePackageClasses={BackendController.class})
	@Import(SecurityConfig.class)
	public static class Config {
		
	}

	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private GameResultRepository repository;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	private GameResult getGameResult(final GameResultType type, final String name, final RockPaperScissorsEnum hand) {

		final GameResult result = new GameResult();
		result.setId(UUID.randomUUID().toString());
		result.setResultType(type);
		result.setWinnerHandValue(hand);
		result.setWinnerPlayerName(name);

		return result;
	}

	private List<GameResult> getResults() {
		final List<GameResult> results = new ArrayList<>();
		results.add(this.getGameResult(GameResultType.DRAW, null, null));
		results.add(this.getGameResult(GameResultType.WINNER, "A", RockPaperScissorsEnum.PAPER));
		results.add(this.getGameResult(GameResultType.WINNER, "B", RockPaperScissorsEnum.SCISSORS));

		return results;
	}

	@Test
	public void testExample() throws Exception {		
		BDDMockito.given(this.repository.findAll()).willReturn(this.getResults());
		
		this.mvc.perform(get("/api/result").with(httpBasic("user", "pwdUser123!")))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].resultType").value("DRAW"))
				.andExpect(jsonPath("$[1].resultType").value("WINNER"))
				.andExpect(jsonPath("$[1].winnerPlayerName").value("A"))
				.andExpect(jsonPath("$[1].winnerHandValue").value("PAPER"))
				.andExpect(jsonPath("$[2].resultType").value("WINNER"))
				.andExpect(jsonPath("$[2].winnerPlayerName").value("B"))
				.andExpect(jsonPath("$[2].winnerHandValue").value("SCISSORS"))
				.andReturn();
				
	}
}
