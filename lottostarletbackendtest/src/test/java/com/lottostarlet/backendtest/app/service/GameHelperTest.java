package com.lottostarlet.backendtest.app.service;

import java.util.HashMap;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.lottostarlet.backendtest.app.domain.GameResultType;
import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.model.GameResult;

/**
 * 
 * @author Davide Martorana
 *
 */
public class GameHelperTest {

	@Test
	public void testScenario1() {
		final Map<String, RockPaperScissorsEnum> mapOfHand = new HashMap<>();
		mapOfHand.put("A", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("B", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("C", RockPaperScissorsEnum.ROCK);
		
		final GameResult result = GameHelper.play(mapOfHand);
		Assertions.assertThat(result.getResultType()).isEqualTo(GameResultType.DRAW);
		Assertions.assertThat(result.getWinnerPlayerName()).isNull();
		Assertions.assertThat(result.getWinnerHandValue()).isNull();
		Assertions.assertThat(result.getId()).isNotNull();
	}
	
	@Test
	public void testScenario2() {
		final Map<String, RockPaperScissorsEnum> mapOfHand = new HashMap<>();
		mapOfHand.put("A", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("B", RockPaperScissorsEnum.PAPER);
		mapOfHand.put("C", RockPaperScissorsEnum.ROCK);
		
		final GameResult result = GameHelper.play(mapOfHand);
		Assertions.assertThat(result.getResultType()).isEqualTo(GameResultType.WINNER);
		Assertions.assertThat(result.getWinnerPlayerName()).isEqualTo("B");
		Assertions.assertThat(result.getWinnerHandValue()).isEqualTo(RockPaperScissorsEnum.PAPER);
		Assertions.assertThat(result.getId()).isNotNull();
	}
	
	@Test
	public void testScenario3() {
		final Map<String, RockPaperScissorsEnum> mapOfHand = new HashMap<>();
		mapOfHand.put("A", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("B", RockPaperScissorsEnum.PAPER);
		mapOfHand.put("C", RockPaperScissorsEnum.SCISSORS);

		final GameResult result = GameHelper.play(mapOfHand);
		Assertions.assertThat(result.getResultType()).isEqualTo(GameResultType.DRAW);
		Assertions.assertThat(result.getWinnerPlayerName()).isNull();
		Assertions.assertThat(result.getWinnerHandValue()).isNull();
		Assertions.assertThat(result.getId()).isNotNull();
	}
	
	@Test
	public void testScenario4() {
		final Map<String, RockPaperScissorsEnum> mapOfHand = new HashMap<>();
		mapOfHand.put("A", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("B", RockPaperScissorsEnum.SCISSORS);
		mapOfHand.put("C", RockPaperScissorsEnum.SCISSORS);


		final GameResult result = GameHelper.play(mapOfHand);
		Assertions.assertThat(result.getResultType()).isEqualTo(GameResultType.WINNER);
		Assertions.assertThat(result.getWinnerPlayerName()).isEqualTo("A");
		Assertions.assertThat(result.getWinnerHandValue()).isEqualTo(RockPaperScissorsEnum.ROCK);
		Assertions.assertThat(result.getId()).isNotNull();
	}
	
	@Test
	public void testScenario5() {
		final Map<String, RockPaperScissorsEnum> mapOfHand = new HashMap<>();
		mapOfHand.put("A", RockPaperScissorsEnum.ROCK);
		mapOfHand.put("B", RockPaperScissorsEnum.PAPER);
		mapOfHand.put("C", RockPaperScissorsEnum.PAPER);

		final GameResult result = GameHelper.play(mapOfHand);
		Assertions.assertThat(result.getResultType()).isEqualTo(GameResultType.DRAW);
		Assertions.assertThat(result.getWinnerPlayerName()).isNull();
		Assertions.assertThat(result.getWinnerHandValue()).isNull();
		Assertions.assertThat(result.getId()).isNotNull();
	}
	
}
