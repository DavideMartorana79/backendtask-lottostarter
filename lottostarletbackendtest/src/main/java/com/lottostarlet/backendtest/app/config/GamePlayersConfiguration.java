package com.lottostarlet.backendtest.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.domain.TurnPlayer;
import com.lottostarlet.backendtest.app.domain.impl.DefaultTurnPlayer;
import com.lottostarlet.backendtest.app.domain.impl.FixedRoundHandStrategy;
import com.lottostarlet.backendtest.app.domain.impl.RandomRoundHandStrategy;

/**
 * Creation of Players
 * 
 * 
 * @author Davide Martorana
 *
 */
@Configuration
public class GamePlayersConfiguration {

	@Bean
	public TurnPlayer first() {
		return new DefaultTurnPlayer("first", new RandomRoundHandStrategy());
	}
	
	@Bean
	public TurnPlayer second() {
		return new DefaultTurnPlayer("second", new RandomRoundHandStrategy());
	}
	
	@Bean
	public TurnPlayer third() {
		return new DefaultTurnPlayer("third", new FixedRoundHandStrategy(RockPaperScissorsEnum.ROCK));
	}
	
}
