package com.lottostarlet.backendtest.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.lottostarlet.backendtest.app.domain.GameResultType;
import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;

@Document(collection="game_result")
public class GameResult {

	@Id
    private String id;

	private GameResultType resultType;
	
	private RockPaperScissorsEnum winnerHandValue;
	
	private String winnerPlayerName;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RockPaperScissorsEnum getWinnerHandValue() {
		return winnerHandValue;
	}

	public void setWinnerHandValue(RockPaperScissorsEnum winnerHandValue) {
		this.winnerHandValue = winnerHandValue;
	}

	
	public GameResultType getResultType() {
		return resultType;
	}

	public void setResultType(GameResultType resultType) {
		this.resultType = resultType;
	}

	public String getWinnerPlayerName() {
		return winnerPlayerName;
	}

	public void setWinnerPlayerName(String winnerPlayerName) {
		this.winnerPlayerName = winnerPlayerName;
	}
	
	
}
