package com.lottostarlet.backendtest.app.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Representation of the played hand.
 * 
 * @author Davide Martorana
 *
 */
public enum RockPaperScissorsEnum {

	ROCK, PAPER, SCISSORS;

	// Map used to establish the winner.
	private static final Map<RockPaperScissorsEnum, RockPaperScissorsEnum> WINS_OVER = new HashMap<>();

	static {
		WINS_OVER.put(ROCK, SCISSORS);
		WINS_OVER.put(SCISSORS, PAPER);
		WINS_OVER.put(PAPER, ROCK);
	}

	/**
	 * Establishes if the current value wins over the other given value.
	 * 
	 * Example: ROCK.winsOver(SCISSORS) returns true.
	 * 
	 * @param other
	 *            - an instance of {@link RockPaperScissorsEnum}
	 * 
	 * @return true if the current value wins, false otherwise.
	 */
	public boolean winsOver(final RockPaperScissorsEnum other) {
		return (WINS_OVER.get(this) == other);
	}
}
