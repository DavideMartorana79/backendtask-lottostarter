package com.lottostarlet.backendtest.app.domain;

/**
 * Result type
 * @author Davide Martorana
 *
 */
public enum GameResultType {

	WINNER,
	DRAW
	
}
