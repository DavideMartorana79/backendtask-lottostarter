package com.lottostarlet.backendtest.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lottostarlet.backendtest.app.model.GameResult;
import com.lottostarlet.backendtest.app.persistence.GameResultRepository;

@RestController
@RequestMapping("/api")
public class BackendController {
	
	@Autowired
	private GameResultRepository repository;
	
	@GetMapping("/result")
	public List<GameResult> getResults() {
		return this.repository.findAll();
	}
	
	@GetMapping("/result/{id}")
	public GameResult getResults(final String id) {
		return this.repository.findById(id);
	}
	
}
