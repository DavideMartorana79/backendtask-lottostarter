package com.lottostarlet.backendtest.app.domain.impl;

import org.apache.commons.lang3.RandomUtils;

import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.domain.RoundStrategy;

/**
 * 
 * @author Davide Martorana
 *
 */
public class RandomRoundHandStrategy implements RoundStrategy{

	private final static RockPaperScissorsEnum[] values = RockPaperScissorsEnum.values();
		
	
	@Override
	public RockPaperScissorsEnum getHandValue() {
		final int random = RandomUtils.nextInt(0, values.length);		
		return values[random];
	}
	
}
