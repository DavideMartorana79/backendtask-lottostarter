package com.lottostarlet.backendtest.app.domain;

/**
 * Defines the strategy used to choose for each hand the value to play. 
 * 
 * @author Davide Martorana
 *
 */
public interface RoundStrategy {

	/**
	 * Returns the instance that the current strategy chose.
	 *  
	 * @return an instance of {@link RockPaperScissorsEnum}
	 */
	RockPaperScissorsEnum getHandValue();
}
