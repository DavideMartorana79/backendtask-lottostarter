package com.lottostarlet.backendtest.app.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.domain.TurnPlayer;
import com.lottostarlet.backendtest.app.model.GameResult;
import com.lottostarlet.backendtest.app.persistence.GameResultRepository;

@Component
public class BackendService {

	private static final Logger LOG = LogManager.getLogger();

	@Value("${app.game.rounds}")
	private int numberOfRounds;
	
	@Autowired
	private List<TurnPlayer> players;
	
	@Autowired
	private GameResultRepository repository;

	private final AtomicInteger counter = new AtomicInteger(0);

	@Scheduled(initialDelayString="${app.schedule.delay}", fixedRateString="${app.schedule.rate}")
	public void initialize() {
		
		if(this.counter.getAndIncrement() < this.numberOfRounds) {
			final Map<String, RockPaperScissorsEnum> mapOfHand = this.players.stream()
					.collect(Collectors.toMap(TurnPlayer::getPlayerName, TurnPlayer::getResultOfTurn));
			
			final GameResult result = GameHelper.play(mapOfHand);
			final GameResult savedValue = this.repository.save(result);
			LOG.debug("Saved Value: {}", savedValue);
		}
	}
		
}
