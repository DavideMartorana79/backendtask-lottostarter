package com.lottostarlet.backendtest.app.service;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.lottostarlet.backendtest.app.domain.GameResultType;
import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.model.GameResult;
import java.util.UUID;
/**
 * Game Result Helper
 * 
 * @author Davide Martorana
 *
 */
public final class GameHelper {

	private GameHelper() {
		throw new IllegalAccessError("This class cannot be instanciated at Runtime");
	}


	/**
	 *  Calculate the Game result according with a given hand.
	 *  
	 * @param mapOfHand
	 *            - a map where the key is the UNIQUE name of the player and the
	 *            value is the {@link RockPaperScissorsEnum} played.
	 * @return an instance of {@link GameResult} that contains the result of
	 *         this round
	 */
	public static final GameResult play(final Map<String, RockPaperScissorsEnum> mapOfHand) {

		Entry<String, RockPaperScissorsEnum> winner = null;

		for (final Entry<String, RockPaperScissorsEnum> currentEntry : mapOfHand.entrySet()) {
			if (GameHelper.isTheWinner(currentEntry, mapOfHand.entrySet())) {
				winner = currentEntry;
				break;
			}
		}

		final GameResultType type = (winner == null) ? GameResultType.DRAW : GameResultType.WINNER;
		final String name = (winner == null) ? null : winner.getKey();
		final RockPaperScissorsEnum hand = (winner == null) ? null : winner.getValue();

		final GameResult result = new GameResult();
		result.setId(UUID.randomUUID().toString());
		result.setResultType(type);
		result.setWinnerHandValue(hand);
		result.setWinnerPlayerName(name);

		return result;
	}

	private static final boolean isTheWinner(final Entry<String, RockPaperScissorsEnum> valueToTest,
			final Set<Entry<String, RockPaperScissorsEnum>> otherEntries) {
		boolean finalResult = true;
		for (final Entry<String, RockPaperScissorsEnum> currentEntry : otherEntries) {
			if (currentEntry.getKey() == valueToTest.getKey()) {
				continue;
			} else if (!valueToTest.getValue().winsOver(currentEntry.getValue())) {
				finalResult = false;
				break;
			}

		}

		return finalResult;
	}
}
