package com.lottostarlet.backendtest.app.domain.impl;

import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.domain.RoundStrategy;

/**
 * 
 * @author Davide Martorana
 *
 */
public class FixedRoundHandStrategy implements RoundStrategy {

	private final RockPaperScissorsEnum handValue;

	public FixedRoundHandStrategy(RockPaperScissorsEnum handValue) {
		this.handValue = handValue;
	}
	
	@Override
	public RockPaperScissorsEnum getHandValue() {
		return this.handValue;
	}
	
}
