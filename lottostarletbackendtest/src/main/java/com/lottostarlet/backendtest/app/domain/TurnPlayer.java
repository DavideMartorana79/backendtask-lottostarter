package com.lottostarlet.backendtest.app.domain;

/**
 * Turn player 
 * 
 * @author Davide Martorana
 *
 */
public interface TurnPlayer {

	/**
	 * Returns an instance of {@link RockPaperScissorsEnum} that is the value of the hand for that turn.
	 * For each hand, this value can change.
	 *  
	 * @return an instance of {@link RockPaperScissorsEnum}
	 */
    RockPaperScissorsEnum getResultOfTurn();
    
    /**
     * Player name
     *
     * @return the player name.
     */
    String getPlayerName();
}
