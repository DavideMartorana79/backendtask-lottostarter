package com.lottostarlet.backendtest.app.domain.impl;

import com.lottostarlet.backendtest.app.domain.RockPaperScissorsEnum;
import com.lottostarlet.backendtest.app.domain.RoundStrategy;
import com.lottostarlet.backendtest.app.domain.TurnPlayer;

/**
 * Default implementation of {@link TurnPlayer}.
 * 
 * @author Davide Martorana
 *
 */
public class DefaultTurnPlayer implements TurnPlayer {

	private final RoundStrategy strategy;
	private final String name;
	
	public DefaultTurnPlayer(final String name, final RoundStrategy strategy) {
		this.strategy = strategy;
		this.name = name;
	}

	@Override
	public RockPaperScissorsEnum getResultOfTurn() {
		return this.strategy.getHandValue();
	}
	
	/**
	 * Player name
	 * 
	 * @return player name
	 */
	public String getPlayerName() {
		return this.name;
	}
}
